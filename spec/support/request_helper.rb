module SpecSupport
  module RequestHelper
    def json_payload
      @payload ||= begin
        if response.body.present?
          payload = JSON.parse(response.body)
          payload = payload.deep_symbolize_keys if payload.is_a?(Hash)
          payload = payload.map(&:deep_symbolize_keys) if payload.is_a?(Array)
          payload
        end
      end
    end
  end
end
