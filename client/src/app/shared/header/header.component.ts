import { Component } from "@angular/core";

@Component({
  selector: "app-header",
  template: `
    <mat-toolbar color="primary">
      <mat-toolbar-row>
        <a mat-button [routerLink]="['/']">Recipes</a>
      </mat-toolbar-row>
    </mat-toolbar>
  `
})
export class HeaderComponent {}
