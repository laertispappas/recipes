require "rails_helper"

describe RecipesController, type: :request do

  describe "GET /api/recipes" do
    let(:do_request) { get "/api/recipes" }

    it "fetches the recipes with the correct query fields" do
      expect(Repositories[:recipes]).to receive(:all).
        with(fields: ["title", "description", "tags"]).
        and_return([Recipe.new])

      do_request
    end

    context "when no recipes are found" do
      before { do_request }

      it { expect(json_payload).to be_empty }
      it { expect(response.status).to eq 200 }
    end

    context "when recipes are found" do
      let(:recipes) {
        [
          build(:recipe),
          build(:recipe, :with_tags)
        ]
      }

      before do
        Repositories[:recipes].add(recipes[0])
        Repositories[:recipes].add(recipes[1])

        do_request
      end

      it { expect(response.status).to eq 200 }

      it "renders the correct payload" do
        expected_response = [
          {
            id: recipes[0].id,
            title: recipes[0].title,
            description: recipes[0].description,
            tags: []
          },
          {
            id: recipes[1].id,
            title: recipes[1].title,
            description: recipes[1].description,
            tags: [
              {
                name: recipes[1].tags[0].name
              },
              {
                name: recipes[1].tags[1].name
              }
            ]
          }
        ]

        expect(json_payload).to eq expected_response
      end
    end
  end

  describe "GET /api/recipes/:ID" do
    let(:do_request) { get "/api/recipes/#{id}" }

    context "when the recipe is not found" do
      before { do_request }

      let(:id) { "MISSING_ID" }

      it { expect(response.status).to eq 404 }
      it { expect(json_payload).to be_empty }
    end

    context "when a recipe id found" do
      let(:id) { recipe.id }

      before do
        Repositories[:recipes].add(recipe)
        do_request
      end

      context "with a recipe that contains all attributes" do
        let(:recipe) do
          build(:recipe, :with_tags, :with_chef, :with_image)
        end

        it { expect(response.status).to eq 200 }

        it "returns the correct json payload" do
          expected_payload = {
            id: recipe.id,
            title: recipe.title,
            description: recipe.description,
            tags: [
              { name: recipe.tags[0].name },
              { name: recipe.tags[1].name }
            ],
            chef: { name: recipe.chef.name },
            image: { url: recipe.asset.url }
          }

          expect(json_payload).to eq expected_payload
        end
      end

      context "with a recipe that contains minimal attributes" do
        let(:recipe) do
          Recipe.new(id: "1", title: "some title", description: "some description")
        end

        it { expect(response.status).to eq 200 }

        it "returns the correct json payload" do
          expected_payload = {
            id: recipe.id,
            title: recipe.title,
            description: recipe.description,
            tags: [],
            chef: nil,
            image: nil
          }

          expect(json_payload).to eq expected_payload
        end
      end
    end
  end
end
