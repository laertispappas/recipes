class Recipe < BaseModel
  attr_accessor :id
  attr_accessor :title, :description, :tags, :asset, :chef

  def initialize(*args)
    super

    @tags ||= []
  end
end
