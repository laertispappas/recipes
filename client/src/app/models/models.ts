// I put all the models in one file for simplicity

export class Tag {
  name: string;
}

export class Chef {
  name: string;
}

export class Image {
  url: string;
}

export class Recipe {
  id: string;
  title: string;
  description: string;
  tags: Tag[];
  chef: Chef;
  image: Image;
}
