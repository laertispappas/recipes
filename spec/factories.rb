FactoryBot.define do
  factory :recipe do
    sequence(:id) { |n| "#{n}" }

    sequence(:title) { |n| "#{n} - Recipe title" }
    description { "a recipe description" }
  end

  trait :with_tags do
    tags { build_list :tag, 2 }
  end

  trait :with_chef do
    chef
  end

  trait :with_image do
    asset
  end

  factory :tag do
    sequence(:name) { |n| "#{n} - tag name" }
  end

  factory :chef do
    sequence(:name) { |n| "#{n} - Chef" }
  end

  factory :asset do
    sequence(:url) { |n| "#{n} - Image url" }
  end
end
