require "rails_helper"

describe Recipe, type: :model do
  let(:recipe) {
    Recipe.new(
      id: "1",
      title: "a recipe",
      description: "descr",
      tags: tags,
      chef: chef,
      asset: asset)
  }

  let(:tags) { nil }
  let(:chef) { nil }
  let(:asset) { nil }

  it { is_expected.to be_a BaseModel }

  it { expect(recipe.id).to eq "1" }
  it { expect(recipe.title).to eq "a recipe" }
  it { expect(recipe.description).to eq "descr" }
  it { expect(recipe.tags).to eq [] }
  it { expect(recipe.chef).to be nil }
  it { expect(recipe.asset).to be nil }

  describe "contruction with all details" do
    let(:tags) { [Tag.new(name: "name")] }
    let(:chef) { Chef.new(name: "L") }
    let(:asset) { Asset.new(url: "http://someurl") }

    it { expect(recipe.tags).to eq tags }
    it { expect(recipe.chef).to eq chef }
    it { expect(recipe.asset).to eq asset }
  end
end
