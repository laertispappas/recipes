class RecipesRepository
  attr_reader :gateway

  def initialize(gateway:)
    @gateway = gateway
  end

  def all(fields: )
    gateway.recipes(fields: fields).map(&method(:to_entity))
  end

  def get(id:)
    to_entity(gateway.recipe(id: id))
  end

  private

  def to_entity(entry)
    Recipe.new(
      id: entry.id,
      title: entry.title,
      description: entry.description,
      tags: map_tags(entry),
      chef: map_chef(entry),
      asset: map_asset(entry)
    )
  end

  def map_tags(entry)
    return [] unless entry.respond_to?(:tags)
    return [] unless entry.tags.present?

    entry.tags.map { |entry| Tag.new(name: entry.name) }
  end

  def map_chef(entry)
    return nil unless entry.respond_to?(:chef)

    Chef.new(name: entry.chef.name)
  end

  def map_asset(entry)
    return nil unless entry.respond_to?(:photo)

    Asset.new(url: entry.photo.url)
  end
end
