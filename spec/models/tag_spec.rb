require "rails_helper"

describe Tag, type: :model do
  let(:tag) { Tag.new(name: "super tag") }

  it { expect(tag.name).to eq "super tag" }
end
