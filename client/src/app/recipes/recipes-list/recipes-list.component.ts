import { Component, OnInit } from "@angular/core";
import { RecipeService } from "src/app/core/services/recipe.service";
import { Observable } from "rxjs";
import { Recipe } from "src/app/models/models";

@Component({
  selector: "app-recipes-list",
  templateUrl: "./recipes-list.component.html",
  styleUrls: ["./recipes-list.component.scss"]
})
export class RecipesListComponent implements OnInit {
  recipes$: Observable<Recipe[]>;

  constructor(private service: RecipeService) {}

  ngOnInit() {
    this.recipes$ = this.service.getRecipes();
  }
}
