require "rails_helper"

describe Repositories, type: :model do
  let(:recipes_repo) { Repositories[:recipes] }

  it { expect(recipes_repo).to be_a InMemoryRecipesRepository }

  describe "#all" do
    it { expect(recipes_repo.all).to be_empty }

    context "when entities exist" do
      let(:recipe) { Recipe.new(id: 1) }
      before { recipes_repo.add(recipe) }

      it { expect(recipes_repo.all).to eq ([recipe]) }
    end
  end

  describe "#get" do
    it "returns nil when no entity is found" do
      recipe = Recipe.new(id: "some id")
      recipes_repo.add recipe

      expect(recipes_repo.get(id: "missing")).to be nil
    end

    it "returns the entity when it is found" do
      recipe = Recipe.new(id: 1)
      recipes_repo.add recipe

      expect(recipes_repo.get(id: recipe.id)).to eq recipe
    end
  end
end
