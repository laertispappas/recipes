class RecipeDetailsSerializer < BaseSerializer
  def as_json(*)
    BaseRecipeSerializer.new(object).as_json.merge(details)
  end

  private

  def details
    {
      chef: object.chef.as_json,
      image: object.asset.as_json
    }
  end
end
