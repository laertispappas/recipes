import { Component, OnInit, OnDestroy } from "@angular/core";
import { RecipeService } from "src/app/core/services/recipe.service";
import { Observable, Subscription } from "rxjs";
import { Recipe } from "src/app/models/models";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-recipe-details",
  templateUrl: "./recipe-details.component.html",
  styleUrls: ["./recipe-details.component.scss"]
})
export class RecipeDetailsComponent implements OnInit, OnDestroy {
  recipe$: Observable<Recipe>;
  private routeSub: Subscription;

  constructor(private route: ActivatedRoute, private service: RecipeService) {}

  ngOnInit() {
    this.routeSub = this.route.params.subscribe(params => {
      const recipeId = params.id;
      this.recipe$ = this.service.getRecipe(recipeId);
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
}
