class ContentfulClient
  attr_reader :client

  def initialize
    @client = Contentful::Client.new(
      space: ENV.fetch("CONTENTFUL_SPACE_ID"),
      access_token: ENV.fetch("CONTENTFUL_TOKEN")
    )
  end

  def recipes(fields: nil)
    if fields.present?
      select = parse_fields(fields)

      client.entries(content_type: 'recipe', select: select)
    else
      client.entries(content_type: 'recipe')
    end
  end

  def recipe(id:)
    client.entry(id)
  end

  private

  def parse_fields(fields)
    fields.map { |f| "fields.#{f}" }
  end
end
