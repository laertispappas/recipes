require "rails_helper"

describe RecipesRepository, type: :model do
  let(:gateway) { double }
  let(:repo) { RecipesRepository.new(gateway: gateway) }

  TagAEntry = OpenStruct.new(name: "tag a")
  TagBEntry = OpenStruct.new(name: "tag b")
  SomeChefEntry = OpenStruct.new(name: "Chef A")
  SomePhotoEntry = OpenStruct.new(url: "some image url")

  MinimalEntry = Struct.new(:id, :title, :description).new("1", "recipe 1", "recipe 1 descr")
  FullEntry = Struct.new(:id, :title, :description, :tags, :chef, :photo).new(
    "2", "recipe full 2", "recipe 2 full descr",
    [
      TagAEntry, TagBEntry
    ],
    SomeChefEntry,
    SomePhotoEntry
  )

  describe "#all" do
    let(:entries) do
      [
        MinimalEntry,
        FullEntry
      ]
    end

    let(:fields) { [] }

    before do
      allow(gateway).to receive(:recipes).with(fields: fields).and_return(entries)
    end

    it "maps the fetched remote api content to the internal recipe DTO" do
      recipes = repo.all(fields: fields)

      expect(recipes.size).to eq 2

      expect(recipes[0].title).to eq entries[0].title
      expect(recipes[0].description).to eq entries[0].description
      expect(recipes[0].id).to eq entries[0].id
      expect(recipes[0].tags).to eq []
      expect(recipes[0].asset).to be nil
      expect(recipes[0].chef).to be nil

      expect(recipes[1].title).to eq entries[1].title
      expect(recipes[1].description).to eq entries[1].description
      expect(recipes[1].id).to eq entries[1].id

      expected_tags = [
        Tag.new(name: TagAEntry.name),
        Tag.new(name: TagBEntry.name)
      ]
      expect(recipes[1].tags).to eq expected_tags

      expect(recipes[1].chef).to eq Chef.new(name: SomeChefEntry.name)
      expect(recipes[1].asset).to eq Asset.new(url: SomePhotoEntry.url)
    end
  end

  describe "#get" do
    it "maps the miniaml entry to the internal Recipe DTO" do
      allow(gateway).to receive(:recipe).with(id: "some-id").and_return(MinimalEntry)
      recipe = repo.get(id: "some-id")

      expect(recipe.title).to eq MinimalEntry.title
      expect(recipe.description).to eq MinimalEntry.description
      expect(recipe.id).to eq MinimalEntry.id
      expect(recipe.tags).to eq []
      expect(recipe.asset).to be nil
      expect(recipe.chef).to be nil
    end

    it "map the full entry to internal Recipe DTO" do
      allow(gateway).to receive(:recipe).with(id: "some-id").and_return(FullEntry)
      recipe = repo.get(id: "some-id")

      expect(recipe.id).to eq FullEntry.id
      expect(recipe.title).to eq FullEntry.title
      expect(recipe.description).to eq FullEntry.description
      expected_tags = [
        Tag.new(name: TagAEntry.name),
        Tag.new(name: TagBEntry.name)
      ]
      expect(recipe.tags).to eq expected_tags
      expect(recipe.chef).to eq Chef.new(name: SomeChefEntry.name)
      expect(recipe.asset).to eq Asset.new(url: SomePhotoEntry.url)

    end
  end
end
