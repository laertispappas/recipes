import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RecipesListComponent } from "./recipes/recipes-list/recipes-list.component";
import { RecipeDetailsComponent } from "./recipes/recipe-details/recipe-details.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "/recipes",
    pathMatch: "full"
  },
  {
    path: "recipes",
    component: RecipesListComponent
  },
  {
    path: "recipes/:id",
    component: RecipeDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
