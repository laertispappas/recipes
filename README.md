# Marley Spoon Recipes

## Approach

The app consists of 2  parts:

* An API server build in rails
* A simple front end application built using angular. The client application lives under `/client` folder

## API dev approach

I concentrated on the actual use case instead of adding the remote persistence layer into my code from the beginning. The remote contenful API implementation was added in the end without any changes in the rest of the application (see infrustracture layer for more details). 

I still miss one small thing to test the integration on non test environments. This can be easily added by testing the app in a different env like dev, integration or whatever. 

### App structure:

* `Models`: Since the app does not have much logic the models folder acts as the main folder to hold the DTOs in our application. This means that the response from the remote contentful API (or whatever remote service we use) will be trnslated internally to those models. I used active mode here to model them. We could also use plain `Struct` classes, `POROs` or even `Dry::Value`. I went with active model without much thinking (see also my notes at the end regarding the models)
* `Infrastructure`: This layer holds all the implementation details about our infrastructure layer. This would be remote api gateways, repositories, database, alerting etc. In our example I hold the actual repositories to access the remote content and the in memory repository that I use for the unit tests. The actual repo implementation lives under `RecipesRepository` this layer also acts as anti corruption layer for the application since it decouples external data and returns only internal domain data.
* `Serializers`: Serialize DTOs or any entities if we had and expose to the external world. Someone can argue if we need them at this context but in my opinion I think yes although the use case here is pretty simple.


## Front end dev approach

Since it was an index view and a show details view. I didn't put much effort here but in a nutshell we have:

* Model our entities in the front end as well.
* Create an recipes http service in order to fetch the recipes from the API (a list or a single one for the details page).
* The service returns an Observable so a component can subscribe to that stream.
* Create 2 components (2 pages): A recipes-list component and a recipe-details component.
* We also use angular router so the main app component will render the above components based on the route that we are (/recipes or /recipes/:id)
* The service created before is injected to each component in order to fetch the related details (IoC).
* In the view we subscribe to the observable and show the relevant page.

We could use NgRx but for this simple scenario would be an overkill. Unfortunately tests are missing here as well.


## Other thoughts

* Localization is not taken into account
* API rate limits is not taken into account
* Caching API response is not taken into account in order to avoid repetitive calls and increase performance
* API keys (.env file for CONTENTFUL API) should be encrypted in a real world use case
* Rubocop is not added on purpose
* Pagination is not taken into account
* I've modeled each entity in it's own class. A simplified recipe DTO would be more appropriate for this example since it's a simple and small one. However having it normalized would be more flexible for future extensions and since the maintainability is not a problem with both approaches I kept the current one.
* Acceptance / integration tests are not added. This would be either done via Capybara or other frameworks like Protractor (directly from angular app) or cypress.
* Dockerize applications


## Run the app

Rails API:

* cd /ROOT_FOLDER
* bundle install
* bundle exec rails server
* This will open the API running on port 3000 as usual

Angular app: See also [client/README](/client/README.md) on how to setup the Angular application

* cd /ROOT_FOLDER/client
* npm install
* ng serve
* The above will run an angular dev process on port 4200

Navigate to http://localhost:4200

## Run tests

For the API:
* bundle exec rspec spec 

For the Front end:

* No tests here

Acceptance / Integration tests:

* No tests here as well
