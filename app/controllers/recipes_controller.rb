class RecipesController < ApplicationController
  def index
    recipes = repo.all(fields: ["title", "description", "tags"])

    render json: CollectionSerializer.new(collection: recipes, serializer: BaseRecipeSerializer)
  end

  def show
    recipe = repo.get(id: params[:id])

    if recipe
      render json: RecipeDetailsSerializer.new(recipe)
    else
      render json: {}, status: 404
    end
  end

  private

  def repo
    Repositories[:recipes]
  end
end
