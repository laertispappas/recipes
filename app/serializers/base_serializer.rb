class BaseSerializer
  attr_reader :object

  def initialize(object)
    @object = object
  end

  def as_json(*)
    raise NotImplementedError
  end
end
