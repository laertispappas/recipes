class BaseModel
  include ActiveModel::Model

  def ==(other)
    instance_values == other.instance_values
  end
end
