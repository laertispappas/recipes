class InMemoryRecipesRepository
  def initialize
    @entities = {}
  end

  def clear!
    @entities = {}
  end

  def add(entity)
    @entities[entity.id] = entity
  end

  def all(fields: [])
    @entities.map { |_id, entity| entity }
  end

  def get(id:)
    @entities[id]
  end
end
