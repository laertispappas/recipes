class BaseRecipeSerializer < BaseSerializer
  def as_json(*)
    {
      id: object.id,
      title: object.title,
      description: object.description,
      tags: object.tags.as_json
    }
  end
end
