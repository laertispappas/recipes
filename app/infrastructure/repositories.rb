class Repositories
  RepositoryNotFoundError = Class.new(StandardError)

  @repositories = {}

  def self.register(name, gateway)
    @repositories[name] = gateway
  end

  def self.[](name)
    @repositories[name] || raise(RepositoryNotFoundError)
  end

  private

  def initialize; end
end

if Rails.env.test?
  Repositories.register(:recipes, InMemoryRecipesRepository.new)
else
  Repositories.register(:recipes, RecipesRepository.new(gateway: ContentfulClient.new))
end
