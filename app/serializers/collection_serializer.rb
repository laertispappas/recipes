class CollectionSerializer
  def initialize(collection:, serializer:)
    @collection = collection
    @serializer = serializer
  end

  def as_json(*)
    @collection.map do |recipe|
      @serializer.new(recipe).as_json
    end
  end
end
