require "rails_helper"

describe ContentfulClient, type: :model do
  let(:wrapper) { described_class.new }

  it { expect(wrapper.client).to be_a Contentful::Client }


  describe "#recipes" do
    context "when no fields are given" do
      it "makes the correct call to the remote api" do
        expect(wrapper.client).to receive(:entries).with(content_type: "recipe")

        wrapper.recipes(fields: [])
      end

      it "returns the remote api content" do
        recipes = double("Contentful::Array")
        allow(wrapper.client).to receive(:entries).
          with(content_type: "recipe").and_return(recipes)

        res = wrapper.recipes(fields: [])
        expect(res).to eq recipes
      end
    end

    context "when the fields are given" do
      it "makes the correct call to the remote api" do
        fields = %w(title description tags)

        expect(wrapper.client).to receive(:entries).with(
          content_type: "recipe",
          select: ['fields.title', 'fields.description', 'fields.tags'])

        wrapper.recipes(fields: fields)
      end

      it "returns all recipes from the remote API" do
        recipes = double("Contentful::Array")
        fields = %w(title description tags)

        allow(wrapper.client).to receive(:entries).with(
          content_type: "recipe",
          select: ['fields.title', 'fields.description', 'fields.tags']).
          and_return(recipes)

        res = wrapper.recipes(fields: fields)
        expect(res).to eq recipes
      end

      it "raises an error when non permitted fields are requested"
    end
  end

  describe "#recipe" do
    let(:id) { "someid" }
    it "delegates the call to the client" do
      expect(wrapper.client).to receive(:entry).with(id)

      wrapper.recipe(id: id)
    end
  end
end
